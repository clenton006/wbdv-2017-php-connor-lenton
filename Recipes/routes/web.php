<!--
---------------------------------------------------------------------
Author: Connor Lenton
Date: March 10, 2017
Course: SAIT PHP
Assignment 1
---------------------------------------------------------------------
-->


<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//find a way to add ingredients to a recipe
//ingredient table
//could have an option to select how many ingredients and/or steps there are
//this will then generate a form with that number of things(loop)
//will get the values from the vable and put them back into the form
//shouldnt need database for that

Route::get('/', "recipeController@home");
Route::post('/', "recipeController@postRecipe");
Route::get('/user', "recipeController@user");

Auth::routes();

Route::get('/home', 'HomeController@index');
