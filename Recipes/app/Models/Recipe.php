<!--
---------------------------------------------------------------------
Author: Connor Lenton
Date: March 10, 2017
Course: SAIT PHP
Assignment 1
---------------------------------------------------------------------
-->
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Recipe extends Model
{
    public function getUser(){
        return User::find($this->user_id);
    }
}
