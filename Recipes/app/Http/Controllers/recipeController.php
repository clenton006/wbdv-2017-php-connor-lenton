<!--
---------------------------------------------------------------------
Author: Connor Lenton
Date: March 10, 2017
Course: SAIT PHP
Assignment 1
---------------------------------------------------------------------
-->
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Recipe;
use App\User;

class recipeController extends Controller
{
    public function home(){
        $data = [
            "recipes" => Recipe::all()  
        ];
        
        return view("recipe", $data);
    }
    
    public function postRecipe(){
        $this->validate(request(), [
            'instructions' => 'required',
            'title' => 'required',
            'time' => 'required|regex:/^\d*\.?\d*$/',
        ], [
            'regex' => 'Time cannot contain letters',
        ]);
        
        $recipe = new Recipe();
        $recipe->instructions = $_POST["instructions"];
        $recipe->title = $_POST["title"];
        $recipe->time = $_POST["time"];
        $recipe->user_id = request()->user()->id;
        $recipe->save();
        return redirect("/");
    }
    
    public function user(){
        $user_id = $_GET["id"];
        $user = User::find($user_id);
        if(is_null($user)){
            die("this user does not exist");
        }
        $recipes = Recipe::where("user_id", $user_id)->get();
        $data = [
            "user" => $user,
            "recipes" => $recipes
        ];
        
        return view("user", $data);
    }

}
