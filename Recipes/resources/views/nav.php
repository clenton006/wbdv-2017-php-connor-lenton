<!--
---------------------------------------------------------------------
Author: Connor Lenton
Date: March 11, 2017
Course: SAIT PHP
Assignment 1
---------------------------------------------------------------------
-->
<nav>
    
    <?php if(Auth::check()) { ?>
        <div class="nav"><a href="."><i class="fa fa-gg inline" aria-hidden="true"></i></a>
            <div class="inline greet"><?php echo "Hello, ".request()->user()->name?></div>
            <form class="inline navControl" action="logout" method="post">
                <?php echo csrf_field(); ?>
                <input class="navInput" type="submit" name="submit" value="Logout">
            </form>
        </div>
    <?php } else { ?>
        <div class="nav"><a href="."><i class="fa fa-gg inline" aria-hidden="true"></i></a>
            <div class="inline greet">Hello, User</div>
            <div class="inline navControl">
                <a class="logInBtn" href="login">Log in</a>
            </div>
        </div>
    <?php } ?>
</nav>