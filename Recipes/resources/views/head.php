<!--
---------------------------------------------------------------------
Author: Connor Lenton
Date: March 11, 2017
Course: SAIT PHP
Assignment 1
---------------------------------------------------------------------
-->
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <style>
        <?php include("styles.css") ?>
    </style>
</head>