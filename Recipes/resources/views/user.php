<!--
---------------------------------------------------------------------
Author: Connor Lenton
Date: March 11, 2017
Course: SAIT PHP
Assignment 1
---------------------------------------------------------------------
-->
<?php include("head.php"); ?>
<?php include("nav.php"); ?>
<h1>User: <?php echo $user->name ?></h1>
<h3><?php echo $user->name ?>'s Recipes</h3>

<div class="recipe-list-container">
    
    <?php foreach($recipes as $recipe) { ?>
        <div class="recipe-container">
            <h4 class="outTitle"><?php echo $recipe->title?></h4>
            <h5 class="outName">Author: <a href="user?id=<?php echo $recipe->getUser()->id ?>"><?php echo $recipe->getUser()->name?></a></h5>
            <div class="outcreated">Posted: <?php echo $recipe->created_at->format("d M Y H:i:s a") ?></div>
            <div class="outTime">Time to Make: <?php echo $recipe->time?>hr</div>
            <h5 class="instructions-header">Instructions</h5>
            <p class="outInstructions"><?php echo $recipe->instructions?></p>
        </div>
    <?php } ?>
    
</div>
