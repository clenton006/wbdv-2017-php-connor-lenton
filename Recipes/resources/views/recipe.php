<!--
---------------------------------------------------------------------
Author: Connor Lenton
Date: March 11, 2017
Course: SAIT PHP
Assignment 1
---------------------------------------------------------------------
-->
<html>
    
    <?php include("head.php"); ?>
    <?php include("nav.php"); ?>
    
    <body>
        <h1>Recipe</h1>
        
        <?php if($errors) { ?>
            <ul class="error">
            <?php foreach ($errors->all() as $message) { ?>
                <li><?php echo $message ?></li>
            <?php } ?>
            </ul>
        <?php } ?>
        
        <?php if(Auth::check()) { ?>
            <form class="inputRecipe" action="" method="post">
                <?php echo csrf_field(); ?>
                Meal Name: <input class="inputText" type="text" name="title" value="<?php echo old("title")?>"><br/>
                Time:(in hours) <input class="inputText" type="text" name="time" value="<?php echo old("time")?>"><br/>
                Instructions: <textarea class="inputText" name="instructions"><?php echo old("instructions")?></textarea><br/>
                <br/>
                <input type="submit" name="submit" value="Submit">
            </form>
        <?php } ?>
        
        <div class="recipe-list-container">
            <?php foreach($recipes as $recipe) { ?>
                <div class="recipe-container">
                    <h4 class="outTitle"><?php echo $recipe->title?></h4>
                    <h5 class="outName">Author: <a href="user?id=<?php echo $recipe->getUser()->id ?>"><?php echo $recipe->getUser()->name?></a></h5>
                    <div class="outcreated">Posted: <?php echo $recipe->created_at->format("d M Y H:i:s a") ?></div>
                    <div class="outTime">Time to Make: <?php echo $recipe->time?>hr</div>
                    <h5 class="instructions-header">Instructions</h5>
                    <p class="outInstructions"><?php echo $recipe->instructions?></p>
                </div>
            <?php } ?>
        </div>
    </body>
</html>